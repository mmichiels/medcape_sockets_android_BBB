package com.miau.gatos.typingbattleonline;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gatos on 07/07/2016.
 */
public class IncidenceModelBBDD {
    @SerializedName("incidenceFound")
    public IncidenceModel incidence;
}
