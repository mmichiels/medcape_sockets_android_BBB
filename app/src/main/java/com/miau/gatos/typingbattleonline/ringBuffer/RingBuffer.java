package com.miau.gatos.typingbattleonline.ringBuffer;

public class RingBuffer{
	
	private static final int BUFFERSIZE = 4096;
	
	private byte[] buffer;
	private int head;
	private int tail;
	private int size;

	public RingBuffer() {
		buffer = new byte[BUFFERSIZE];
		head = 0;
		tail = 0;
		size = 0;
	}
	
	public synchronized int size() {
		return size;
	}
	
	public synchronized void add(byte value) {
		if ((head == tail) && (size == BUFFERSIZE)){
			buffer = new byte[BUFFERSIZE];
			head = 0;
			tail = 0;
			size = 0;
			//throw new ArrayIndexOutOfBoundsException("Ring buffer full");
		} else {
			buffer[head] = value;
			head++;
			head &= 0xFFF;
			size++;
		}
	}
	
	public synchronized byte get() {
		if ((head == tail) && (size == 0)){
			throw new ArrayIndexOutOfBoundsException("Ring buffer empty");
		} else {
			byte value = buffer[tail];
			tail++;
			tail &= 0xFFF;
			size--;
			return value;
		}
	}
	
	public synchronized boolean isFull() {
		return ((head == tail) && (size == BUFFERSIZE));
	}
	
	public synchronized boolean isEmpty() {
		return ((head == tail) && (size == 0));
	}
	
	public synchronized byte valueOfHead() {
		return buffer[head];
	}
	
}
