package com.miau.gatos.typingbattleonline.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.miau.gatos.typingbattleonline.ConfigSocketIO;
import com.miau.gatos.typingbattleonline.IncidencesArrayAdapter;
import com.miau.gatos.typingbattleonline.R;
import com.miau.gatos.typingbattleonline.IncidencesArrayAdapter;
import com.miau.gatos.typingbattleonline.IncidenceModel;
import com.miau.gatos.typingbattleonline.IncidenceModelList;
import com.miau.gatos.typingbattleonline.RecordingModel;
import com.miau.gatos.typingbattleonline.viewer.ViewerStatic;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import io.socket.emitter.Emitter;

public class ListMyIncidencesFragment extends ListFragment {
    private RecordingModel recording;
    private LineChart chart;
    private ArrayList<String> xLabels;

    @Override
    public void onResume() {
        super.onResume();
        configSocketEvents();

        ViewerStatic activity_viewerStatic = (ViewerStatic)getActivity();
        recording = activity_viewerStatic.recording;
        chart = activity_viewerStatic.chart;
        xLabels = activity_viewerStatic.xLabels;

        Log.w("", "Recording id is: " + recording);


        JSONObject data = new JSONObject();
        try {
            data.put("recording_id", recording.get_id());
            ConfigSocketIO.socket.emit("load incidences server", data);
        } catch (JSONException e) {
            System.out.println(e);
        }

    }

    private Context context;
    public ArrayList<IncidenceModel> incidencesList = new ArrayList<IncidenceModel>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ViewerStatic activity_viewerStatic = (ViewerStatic)getActivity();
        recording = activity_viewerStatic.recording;
        Log.w("", "Recording id is: " + recording);

        View view =  inflater.inflate(R.layout.activity_list_my_incidences_fragment, container, false); //o container a null
        //Populate listView, ahora que la vista ya está poblada.
        // Otra forma sería hacer que extienda de fragment o de activity para así poder poblar la lista en onActivityCreated
        this.context = getActivity();
        //populateListView(view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        /*
        Intent myIntent = new Intent(ListMyIncidencesFragment.this.getActivity(), ViewerStatic.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("recording", incidencesList.get(position));
        myIntent.putExtras(bundle);
        startActivity(myIntent);
        */
        String date_string = incidencesList.get(position).getDate_incidence();
        //=======Parse date, set x time labels==============
        SimpleDateFormat h_m_s = new SimpleDateFormat("hh:mm:ss");

        SimpleDateFormat format = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ITALY);
        Date date_date_format;
        try {
            date_date_format = format.parse(date_string);
            date_string = h_m_s.format(date_date_format);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        float xValue =  xLabels.indexOf(date_string);
        chart.fitScreen();
        chart.moveViewToX(xValue);
        chart.zoom(2, 2, xValue, 600);
        chart.centerViewTo(xValue, 600, YAxis.AxisDependency.LEFT);

        chart.invalidate(); // refresh

        //chart.zoom(2, 2, xValue, 700);
    }

    public void configSocketEvents() {
        if (ConfigSocketIO.socket == null) {
            return;
        }
        ConfigSocketIO.socket.on("load incidences client", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                if (getActivity() == null) {
                    return;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];

                        Gson myGson = new Gson();
                        Type listType = new TypeToken<IncidenceModelList>(){}.getType();
                        IncidenceModelList IncidenceModel = myGson.fromJson(data.toString(), IncidenceModelList.class);
                        incidencesList = IncidenceModel.incidences;
                        /*
                        try {
                            incidencesList.clear();
                            JSONArray jArray = (JSONArray)data.get("listRecordings");
                            if (jArray != null) {
                                for (int i=0;i<jArray.length();i++){
                                    incidencesList.add((IncidenceModel)jArray.get(i));
                                }
                            }
                            */
                            IncidencesArrayAdapter myAdapter = new IncidencesArrayAdapter(getActivity().getApplicationContext(), R.layout.item_my_incidence, incidencesList);
                            ListView lv = (ListView) getListView();
                            lv.setAdapter(myAdapter);
                        /*
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
*/

                    }
                });
            }
        });
    }


}