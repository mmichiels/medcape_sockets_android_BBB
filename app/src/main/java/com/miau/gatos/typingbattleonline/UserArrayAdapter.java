package com.miau.gatos.typingbattleonline;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.miau.gatos.typingbattleonline.ui.ListMyGamesFragment;
import com.miau.gatos.typingbattleonline.ui.ProfilePatient;
import com.miau.gatos.typingbattleonline.viewer.Viewer_another_patient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gatos on 05/07/2016.
 */
public class UserArrayAdapter extends ArrayAdapter {
    private Context context;
    private ArrayList<String> patientsList;
    private int resource;
    private LayoutInflater inflater;

    public UserArrayAdapter(Context context, int resource, ArrayList<String> objects) {
        super(context, resource, objects);
        this.context = context;
        this.patientsList = objects;
        this.resource = resource;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if(convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(resource, null);
           //holder.playerImageItem = (ImageView)convertView.findViewById(R.id.playerImageItem);
            holder.patientNameItem = (TextView)convertView.findViewById(R.id.patientNameItem);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        //final ProgressBar progressBar = (ProgressBar)convertView.findViewById(R.id.progressBar);

        //ImageLoader.getInstance().displayImage(movieModelList.get(position).getImage(), holder.ivMovieIcon, new ImageLoadingListener() {

            //holder.playerImageItem.setImageResource(patientsList.get(position).getOpponentPlayerImage());

        holder.patientNameItem.setText(patientsList.get(position));



        final Button buttonDeleteGame = (Button) convertView.findViewById(R.id.buttonDeleteGame);
        final Button watch_broadcast_button = (Button) convertView.findViewById(R.id.watch_broadcast_button);
        final Button view_recordings_button = (Button) convertView.findViewById(R.id.view_recordings_button);



        buttonDeleteGame.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                new AlertDialog.Builder(context)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Delete game")
                        .setMessage("Are you sure you want to delete this game?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                JSONObject data = new JSONObject();
                                try {
                                    data.put("player1Name", "");
                                    ConfigSocketIO.socket.emit("delete game", data);
                                } catch (JSONException e) {
                                    System.out.println(e);
                                }

                            }

                        })
                        .setNegativeButton("No", null)
                        .show();

            }
        });

        watch_broadcast_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                JSONObject data = new JSONObject();
                try {
                    String patient_name = patientsList.get(position);
                    data.put("doctor_name", ConfigSocketIO.user);
                    data.put("patient_name", patient_name);
                    Toast.makeText(context, "Transmission requested to patient: " + patient_name, Toast.LENGTH_SHORT).show();

                    ConfigSocketIO.socket.emit("request transmission server", data);
                    Intent myIntent = new Intent(context, Viewer_another_patient.class);
                    context.startActivity(myIntent);
                } catch (JSONException e) {
                    System.out.println(e);
                }

            }
        });

        view_recordings_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(context, ProfilePatient.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("patient_name", patientsList.get(position));
                myIntent.putExtras(bundle);
                context.startActivity(myIntent);

            }
        });

        return convertView;
        }

    class ViewHolder{
        private ImageView patientImageItem;
        private TextView patientNameItem;
    }

}

