package com.miau.gatos.typingbattleonline.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import io.socket.client.IO;
import io.socket.client.Socket;

import com.miau.gatos.typingbattleonline.R;

public class InitialLogin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial_login);

        final EditText nameGuest = (EditText)  findViewById(R.id.userGuest);
        Button buttonGuest = (Button) findViewById(R.id.buttonGuest);


        buttonGuest.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(InitialLogin.this, MainMenu.class);
                String nameLogged = nameGuest.getText().toString();
                myIntent.putExtra("userLogged", nameLogged);
                startActivity(myIntent);
            }
        });

    }
}
