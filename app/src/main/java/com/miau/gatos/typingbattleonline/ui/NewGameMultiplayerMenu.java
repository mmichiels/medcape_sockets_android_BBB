package com.miau.gatos.typingbattleonline.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.miau.gatos.typingbattleonline.R;

public class NewGameMultiplayerMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_multiplayer_menu);

        Button matchmakingButton = (Button) findViewById(R.id.matchmakingButton);
        Button findPlayerByNameButton = (Button) findViewById(R.id.findPlayerByNameButton);


        matchmakingButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(NewGameMultiplayerMenu.this, MatchmakingSearchingPlayer.class);
                startActivity(myIntent);
            }
        });

        findPlayerByNameButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(NewGameMultiplayerMenu.this, SearchPlayer.class);
                startActivity(myIntent);
            }
        });

    }
}
