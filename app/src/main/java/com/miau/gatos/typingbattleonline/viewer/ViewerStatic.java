package com.miau.gatos.typingbattleonline.viewer;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.miau.gatos.typingbattleonline.ConfigSocketIO;
import com.miau.gatos.typingbattleonline.R;
import com.miau.gatos.typingbattleonline.RecordingModel;
import com.miau.gatos.typingbattleonline.ui.MultiplayerMenu;
import com.miau.gatos.typingbattleonline.ui.SearchPlayer;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.socket.emitter.Emitter;

public class ViewerStatic extends AppCompatActivity {
    public RecordingModel recording;
    private List<Byte> data_received = new ArrayList<Byte>();

    public static final byte PACKET_TAG            = (byte) 0xC0;
    public static final int ADC_CHANNELS           = 8;
    public static final int ADC_SIZE_CHANNEL       = 2;
    public static final int ADC_STATUS_BYTES       = 2; //Actually there are 3 status bytes, but 1 of them has been removed just to make pair the number of bytes
    public static final int ADC_PACKET_SIZE        = ADC_STATUS_BYTES + (ADC_CHANNELS*ADC_SIZE_CHANNEL);
    public static final int PACKET_SIZE            = ADC_PACKET_SIZE;
    public static final int MIN_RING_BUFFER_LENGTH = PACKET_SIZE + 1;

    private TextView date_viewer_static;
    private TextView patient_viewer_static;
    private Button animate_plot_button;

    public LineChart chart;
    private List<Entry> entries_ch1 = new ArrayList<Entry>();
    private List<Entry> entries_ch2 = new ArrayList<Entry>();
    private List<Entry> entries_ch3 = new ArrayList<Entry>();
    private List<Entry> entries_ch4 = new ArrayList<Entry>();
    private List<Entry> entries_ch5 = new ArrayList<Entry>();
    private List<Entry> entries_ch6 = new ArrayList<Entry>();
    private List<Entry> entries_ch7 = new ArrayList<Entry>();
    private List<Entry> entries_ch8 = new ArrayList<Entry>();

    private int indexPlotADC;

    public final ArrayList<String> xLabels = new ArrayList<>();


    @Override
    public void onResume() {
        super.onResume();
        configSocketEvents();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer_static);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            this.recording = (RecordingModel)b.get("recording");
        } else {
            Toast.makeText(this, "Error al obtener el id de la grabación", Toast.LENGTH_SHORT).show();
        }

        date_viewer_static = (TextView) findViewById(R.id.date_viewer_static);
        patient_viewer_static = (TextView) findViewById(R.id.patient_viewer_static);
        animate_plot_button = (Button) findViewById(R.id.animate_plot_button);
        chart = (LineChart) findViewById(R.id.chart);
        chart.setNoDataText("Loading plot...");
        Description description = new Description();
        description.setText("ECG Plot");
        chart.setDescription(description);

        patient_viewer_static.setText("Patient: " + this.recording.getPatient_name());
        date_viewer_static.setText("Date: " + this.recording.getDate());

        JSONObject data = new JSONObject();
        try {
            data.put("recording_path", recording.getRecording());
            ConfigSocketIO.socket.emit("load recording data server", data);
        } catch (JSONException e) {
            System.out.println(e);
        }


        animate_plot_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chart.animateX(10000);
            }
        });

    }


    public void configSocketEvents() {

        ConfigSocketIO.socket.on("load recording data client", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                if (this == null) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //System.out.println("Chunk received");
                        JSONObject data = (JSONObject) args[0];
                        byte[] recvBuff_received = new byte[0];
                        try {
                            recvBuff_received = (byte[])data.get("recording_data");
                            for(byte b : recvBuff_received) {
                                data_received.add(new Byte(b));
                            }

                        while (data_received.size() > ADC_PACKET_SIZE - 1) {
                            double point;
                            switch (get_byte_from_data()) {

                                case PACKET_TAG:
                                    boolean corrupt_packet = false;

                                    byte[] bytes_packet = new byte[ADC_PACKET_SIZE - 1];
                                    for (int i = 0; i < ADC_PACKET_SIZE - 1; i++) {
                                        bytes_packet[i] = get_byte_from_data();
                                        if (bytes_packet[i] == PACKET_TAG) {
                                            corrupt_packet = true;
                                            Log.w("", "Packet corrupt");
                                            break;
                                        }
                                    }
                                    if (!corrupt_packet) {
                                        Log.w("", "OK packet");

                                        int first_byte = bytes_packet[1];
                                        int second_byte = bytes_packet[2];
                                        point = (first_byte << 8 | second_byte) /1 + 700;
                                        // turn your data into Entry objects
                                        entries_ch1.add(new Entry(indexPlotADC, (float) point));


                                        int third_byte = bytes_packet[3];
                                        int fourth_byte = bytes_packet[4];
                                        point = (third_byte << 8 | fourth_byte) /1  + 600;
                                        entries_ch2.add(new Entry(indexPlotADC, (float) point));


                                        int fifth_byte = bytes_packet[5];
                                        int sixth_byte = bytes_packet[6];
                                        point = (fifth_byte << 8 | sixth_byte) /1  + 500;
                                        entries_ch3.add(new Entry(indexPlotADC, (float) point));
                                        //detect_qrs((int)Math.round((fifth_byte << 8 | sixth_byte)));

                                        int seventh_byte = bytes_packet[7];
                                        int eighth_byte = bytes_packet[8];
                                        point = (seventh_byte << 8 | eighth_byte) /1 + 400;
                                        entries_ch4.add(new Entry(indexPlotADC, (float) point));

                                        int byte_9 = bytes_packet[9];
                                        int byte_10 = bytes_packet[10];
                                        point = (byte_9 << 8 | byte_10) /1 + 300;
                                        entries_ch5.add(new Entry(indexPlotADC, (float) point));

                                        int byte_11 = bytes_packet[11];
                                        int byte_12 = bytes_packet[12];
                                        point = (byte_11 << 8 | byte_12) /1 + 200;
                                        entries_ch6.add(new Entry(indexPlotADC, (float) point));

                                        int byte_13 = bytes_packet[13];
                                        int byte_14 = bytes_packet[14];
                                        point = (byte_13 << 8 | byte_14) /1 + 100;
                                        entries_ch7.add(new Entry(indexPlotADC, (float) point));

                                        int byte_15 = bytes_packet[15];
                                        int byte_16 = bytes_packet[16];
                                        point = (byte_15 << 8 | byte_16) /1 + 10;
                                        entries_ch8.add(new Entry(indexPlotADC, (float) point));

                                        indexPlotADC++;

                                    }
                                    break;
                                default:
                                    Log.w("", "Tag missed");
                                    break;
                            }
                        }

                        LineDataSet dataSet1 = new LineDataSet(entries_ch1, "Channel 1(señal interna)"); // add entries to dataset
                        dataSet1.setColor(Color.BLACK);
                        dataSet1.setCircleColor(Color.BLACK);
                        dataSet1.setDrawCircles(false);
                        dataSet1.setLineWidth(2f);
                        dataSet1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        LineDataSet dataSet2 = new LineDataSet(entries_ch2, "Channel 2"); // add entries to dataset
                        dataSet2.setColor(Color.RED);
                        dataSet2.setCircleColor(Color.RED);
                        dataSet2.setDrawCircles(false);
                        dataSet2.setLineWidth(2f);
                        dataSet2.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        LineDataSet dataSet3 = new LineDataSet(entries_ch3, "Channel 3"); // add entries to dataset
                        dataSet3.setColor(Color.RED);
                        dataSet3.setCircleColor(Color.RED);
                        dataSet3.setDrawCircles(false);
                        dataSet3.setLineWidth(2f);
                        dataSet3.setMode(LineDataSet.Mode.CUBIC_BEZIER);

                        LineDataSet dataSet4 = new LineDataSet(entries_ch4, "Channel 4"); // add entries to dataset
                        LineDataSet dataSet5 = new LineDataSet(entries_ch5, "Channel 5"); // add entries to dataset
                        LineDataSet dataSet6 = new LineDataSet(entries_ch6, "Channel 6"); // add entries to dataset
                        LineDataSet dataSet7 = new LineDataSet(entries_ch7, "Channel 7"); // add entries to dataset
                        LineDataSet dataSet8 = new LineDataSet(entries_ch8, "Channel 8"); // add entries to dataset

                        LineData lineData = new LineData(dataSet1, dataSet2,dataSet3, dataSet4, dataSet5, dataSet6, dataSet7, dataSet8);

                        //=========Disable distorsion when zooming===========
                        // enable touch gestures
                        chart.setTouchEnabled(true);

                        chart.setDragDecelerationFrictionCoef(0.9f);

                        // enable scaling and dragging
                        chart.setDragEnabled(true);
                        chart.setScaleEnabled(true);
                        chart.setDrawGridBackground(false);
                        chart.setHighlightPerDragEnabled(true);
                        //=======================================================
                        XAxis xAxis = chart.getXAxis();


                        String date_string = recording.getDate();
                        //=======Parse date, set x time labels==============
                        SimpleDateFormat h_m_s = new SimpleDateFormat("hh:mm:ss");

                        SimpleDateFormat format = new SimpleDateFormat(
                            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ITALY);

                        Date date_date_format;
                        Date addMilliSeconds;
                        try {
                            date_date_format = format.parse(date_string);
                            Calendar calendar = dateToCalendar(date_date_format);
                            for (int i=0;i < entries_ch8.size(); i++) {
                                calendar.add(Calendar.MILLISECOND, 10);
                                addMilliSeconds = calendar.getTime();
                                date_string = h_m_s.format(addMilliSeconds);
                                xLabels.add(date_string);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        //======================================

                        xAxis.setValueFormatter(new IAxisValueFormatter() {
                            @Override
                            public String getFormattedValue(float value, AxisBase axis) {
                                return xLabels.get((int)value);
                            }
                        });
                        // if disabled, scaling can be done on x- and y-axis separately
                        chart.setPinchZoom(true);
                        chart.setData(lineData);

                        //chart.setScaleEnabled(false);
                        //chart.setScaleYEnabled(false);
                        //chart.setScaleXEnabled(false);
                        chart.invalidate(); // refresh

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
            }
        });

    }

    public byte get_byte_from_data() {
        //if ()
        byte return_byte = data_received.get(0);
        data_received.remove(0);

        return return_byte;
    }

    //Convert Date to Calendar
    private Calendar dateToCalendar(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;

    }
}
