package com.miau.gatos.typingbattleonline;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by gatos on 05/07/2016.
 */
public class IncidenceModelList {
    @SerializedName("listIncidences")
    public ArrayList<IncidenceModel> incidences;
}
