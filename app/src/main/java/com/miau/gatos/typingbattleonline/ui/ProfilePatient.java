package com.miau.gatos.typingbattleonline.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.miau.gatos.typingbattleonline.R;

public class ProfilePatient extends AppCompatActivity {
    public String patient_name;
    private TextView patient_name_label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_patient);

        patient_name_label = (TextView) findViewById(R.id.patient_name_profile);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            this.patient_name = b.getString("patient_name").trim();
            patient_name_label.setText("Patient: " + this.patient_name);
        } else {
            Toast.makeText(this, "Error al obtener el nombre del patient_name", Toast.LENGTH_SHORT).show();
        }





    }







}
