package com.miau.gatos.typingbattleonline;

import java.io.Serializable;
import java.util.List;

/**
 * Created by gatos on 05/07/2016.
 */
public class UserModel implements Serializable {


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String[] getPatients() {
        return patients;
    }

    public void setPatients(String[] patients) {
        this.patients = patients;
    }

    private int id;
    private String name;
    private String language;
    private String country;
    private String patients[];


}
