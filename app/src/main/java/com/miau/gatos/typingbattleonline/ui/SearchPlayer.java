package com.miau.gatos.typingbattleonline.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.miau.gatos.typingbattleonline.ConfigSocketIO;
import com.miau.gatos.typingbattleonline.R;
import com.miau.gatos.typingbattleonline.viewer.Viewer_another_patient;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.emitter.Emitter;

public class SearchPlayer extends AppCompatActivity {
    private Context context;
    private TextView playerFoundText;
    private Button playPlayerFound;


    private  TextView opponentTitleResults, phrase1Result, phrase1ResultsMe, phrase1ResultsOpponent,
            phrase2Result, phrase2ResultsMe, phrase2ResultsOpponent;
    private String opponentName;

    @Override
    public void onResume() {
        super.onResume();
        configSocketEvents();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_player);
        this.context = this;

        final EditText searchPlayerText = (EditText)  findViewById(R.id.searchPlayerText);
        final Button searchPlayerButton = (Button) findViewById(R.id.searchPlayerButton);

        playerFoundText = (TextView) findViewById(R.id.playerFoundText);
        playPlayerFound = (Button) findViewById(R.id.playPlayerFound);
        playPlayerFound.setVisibility(View.GONE);


        searchPlayerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String playerToSearch = searchPlayerText.getText().toString().trim();

                JSONObject data = new JSONObject();
                try {
                    data.put("playerName", playerToSearch);
                    ConfigSocketIO.socket.emit("search player", data);
                } catch (JSONException e) {
                    System.out.println(e);
                }
            }
        });

        playPlayerFound.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String playerToSearch = searchPlayerText.getText().toString().trim();

                JSONObject data = new JSONObject();
                try {
                    data.put("doctor_name", ConfigSocketIO.user);
                    data.put("patient_name", playerToSearch);

                    ConfigSocketIO.socket.emit("add patient to doctor server", data);
                } catch (JSONException e) {
                    System.out.println(e);
                }

                data = new JSONObject();
                try {
                    data.put("doctor_name", ConfigSocketIO.user);
                    data.put("patient_name", playerToSearch);
                    Toast.makeText(context, "Transmission requested to patient: " + playerToSearch, Toast.LENGTH_SHORT).show();

                    ConfigSocketIO.socket.emit("request transmission server", data);
                    Intent myIntent = new Intent(context, Viewer_another_patient.class);
                    startActivity(myIntent);
                } catch (JSONException e) {
                    System.out.println(e);
                }

            }
        });

    }


    public void configSocketEvents() {

        ConfigSocketIO.socket.on("player found client", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                if (this == null) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            String playerName = data.getString("playerName");
                            if(playerName.equals("not found")) {
                                playerFoundText.setText("player not found");
                            } else {
                                playerFoundText.setText(playerName);
                                playPlayerFound.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });



    }

}
