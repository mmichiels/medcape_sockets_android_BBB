package com.miau.gatos.typingbattleonline.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.miau.gatos.typingbattleonline.ConfigSocketIO;
import com.miau.gatos.typingbattleonline.UserArrayAdapter;
import com.miau.gatos.typingbattleonline.UserModel;
import com.miau.gatos.typingbattleonline.UserModelList;
import com.miau.gatos.typingbattleonline.R;
import com.miau.gatos.typingbattleonline.viewer.Viewer;
import com.miau.gatos.typingbattleonline.viewer.Viewer_another_patient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.socket.emitter.Emitter;

public class ListMyGamesFragment extends ListFragment {
    @Override
    public void onResume() {
        super.onResume();
        configSocketEvents();
        JSONObject data = new JSONObject();
        try {
            data.put("name", ConfigSocketIO.user);
            ConfigSocketIO.socket.emit("load patients server", data);
        } catch (JSONException e) {
            System.out.println(e);
        }

    }

    private Context context;
    public ArrayList<String> usersList = new ArrayList<String>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        View view =  inflater.inflate(R.layout.activity_list_my_games_fragment, container, false); //o container a null
        //Populate listView, ahora que la vista ya está poblada.
        // Otra forma sería hacer que extienda de fragment o de activity para así poder poblar la lista en onActivityCreated
        this.context = getActivity();
        //populateListView(view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);


        Intent myIntent = new Intent(ListMyGamesFragment.this.getActivity(), ProfilePatient.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("patient_name", usersList.get(position));
        myIntent.putExtras(bundle);
        startActivity(myIntent);




    }

    public void configSocketEvents() {
        if (ConfigSocketIO.socket == null) {
            return;
        }
        ConfigSocketIO.socket.on("load patients client", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                if (getActivity() == null) {
                    return;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        /*
                        Gson myGson = new Gson();
                        Type listType = new TypeToken<UserModelList>(){}.getType();
                        UserModelList UserModel = myGson.fromJson(data.toString(), UserModelList.class);
                        usersList = UserModel.patients;
                        */
                        try {
                            usersList.clear();
                            JSONArray jArray = (JSONArray)data.get("listPatient");
                            if (jArray != null) {
                                for (int i=0;i<jArray.length();i++){
                                    usersList.add(jArray.getString(i));
                                }
                            }
                            UserArrayAdapter myAdapter = new UserArrayAdapter(getActivity().getApplicationContext(), R.layout.item_my_games, usersList);
                            ListView lv = (ListView) getListView();
                            lv.setAdapter(myAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
            }
        });
    }


}