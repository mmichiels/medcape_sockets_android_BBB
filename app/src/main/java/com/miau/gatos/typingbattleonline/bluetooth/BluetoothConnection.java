package com.miau.gatos.typingbattleonline.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class BluetoothConnection {

	public static final int RECV_BUFF_SIZE = 512;

	private BluetoothAdapter mAdapter;
	private BluetoothSocket mSocket;
	private BluetoothDevice mDevice;
	private InputStream mInputStream;
	private OutputStream mOutputStream;
	private byte[] recvBuff = new byte[RECV_BUFF_SIZE];
	private int bytesRead = 0;


	public int getBytesRead() {
		return bytesRead;
	}


	public BluetoothConnection() {
		mAdapter = BluetoothAdapter.getDefaultAdapter();
	}


	public boolean open(UUID uuid) {
		boolean ok = false;
		while (!ok) {
			try {
				mSocket = mDevice.createInsecureRfcommSocketToServiceRecord(uuid);
				//mSocket = mDevice.createRfcommSocketToServiceRecord(uuid);
				mSocket.connect();
				mOutputStream = mSocket.getOutputStream();
				mInputStream = mSocket.getInputStream();
				ok = true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ok = false;
			}
		}
		return true;
	}


	public Boolean close() {
		try {
			if ((mSocket != null) && (mInputStream != null) && (mOutputStream != null)) {
				mOutputStream.close();
				mInputStream.close();
				mSocket.close();
				return true;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}


	public boolean findDevice(Activity activity) {

		if (mAdapter == null) {
			return false;  // No bluetooth adapter available

		} else {

			if (!mAdapter.isEnabled()) {
				Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				activity.startActivityForResult(enableBluetooth, 0);
			}

			Set<BluetoothDevice> pairedDevices = mAdapter.getBondedDevices();

			if (pairedDevices.size() > 0) {
				for (BluetoothDevice device : pairedDevices) {
					String name_device = device.getName();
					if (device.getName().equals("beaglebone-0")) {
						mDevice = device;
						return true;
					}
				}
			}

			return false;

		}

	}


	public synchronized boolean receiveDataIsAvailable() {
		try {
			if (mInputStream.available() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}


	public synchronized byte[] receiveData() {
		try {
			bytesRead = mInputStream.available();
			mInputStream.read(recvBuff);
			return recvBuff;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	public synchronized void sendData(int data) {
		try {
			if (mOutputStream != null) {
				mOutputStream.write(data);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
