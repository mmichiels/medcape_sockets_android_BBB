package com.miau.gatos.typingbattleonline;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by gatos on 05/07/2016.
 */
public class IncidencesArrayAdapter extends ArrayAdapter {
    private Context context;
    private ArrayList<IncidenceModel> incidencesList;
    private int resource;
    private LayoutInflater inflater;

    public IncidencesArrayAdapter(Context context, int resource, ArrayList<IncidenceModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.incidencesList = objects;
        this.resource = resource;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if(convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(resource, null);
           //holder.playerImageItem = (ImageView)convertView.findViewById(R.id.playerImageItem);
            holder.date_incidence = (TextView)convertView.findViewById(R.id.date_incidence);
            holder.incidence_type = (TextView)convertView.findViewById(R.id.incidence_type);
            holder.value_incidence = (TextView)convertView.findViewById(R.id.value_incidence);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        //final ProgressBar progressBar = (ProgressBar)convertView.findViewById(R.id.progressBar);

        //ImageLoader.getInstance().displayImage(movieModelList.get(position).getImage(), holder.ivMovieIcon, new ImageLoadingListener() {

            //holder.playerImageItem.setImageResource(patientsList.get(position).getOpponentPlayerImage());

        holder.date_incidence.setText(incidencesList.get(position).getDate_incidence());
        holder.incidence_type.setText(incidencesList.get(position).getType_incidence() + ": ");
        holder.value_incidence.setText(String.valueOf(incidencesList.get(position).getValue_incidence()) + "ppm");


        return convertView;
        }

    class ViewHolder{
        private TextView date_incidence;
        private TextView incidence_type;
        private TextView value_incidence;
    }

}

