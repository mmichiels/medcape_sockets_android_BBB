package com.miau.gatos.typingbattleonline;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by gatos on 05/07/2016.
 */
public class RecordingModel implements Serializable {



    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getRecording() {
        return recording;
    }

    public void setRecording(String recording) {
        this.recording = recording;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }




    private String _id;
    private String patient_id;
    private String recording;
    private String date;
    private String patient_name;

}
