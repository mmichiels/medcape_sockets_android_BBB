package com.miau.gatos.typingbattleonline.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.miau.gatos.typingbattleonline.ConfigSocketIO;
import com.miau.gatos.typingbattleonline.R;
import com.miau.gatos.typingbattleonline.RecordingArrayAdapter;
import com.miau.gatos.typingbattleonline.RecordingModel;
import com.miau.gatos.typingbattleonline.RecordingModelList;
import com.miau.gatos.typingbattleonline.viewer.ViewerStatic;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import io.socket.emitter.Emitter;

public class ListMyRecordingsFragment extends ListFragment {
    private String patient_name;

    @Override
    public void onResume() {
        super.onResume();
        configSocketEvents();

        ProfilePatient activity_profile_patient = (ProfilePatient)getActivity();
        patient_name = activity_profile_patient.patient_name;
        Log.w("", "Patient is" + patient_name);


        JSONObject data = new JSONObject();
        try {
            data.put("patient_name", patient_name);
            ConfigSocketIO.socket.emit("load recordings server", data);
        } catch (JSONException e) {
            System.out.println(e);
        }

    }

    private Context context;
    public ArrayList<RecordingModel> recordingsList = new ArrayList<RecordingModel>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ProfilePatient activity_profile_patient = (ProfilePatient)getActivity();
        patient_name = activity_profile_patient.patient_name;
        Log.w("", "Patient is" + patient_name);

        View view =  inflater.inflate(R.layout.activity_list_my_recordings_fragment, container, false); //o container a null
        //Populate listView, ahora que la vista ya está poblada.
        // Otra forma sería hacer que extienda de fragment o de activity para así poder poblar la lista en onActivityCreated
        this.context = getActivity();
        //populateListView(view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);


        Intent myIntent = new Intent(ListMyRecordingsFragment.this.getActivity(), ViewerStatic.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("recording", recordingsList.get(position));
        myIntent.putExtras(bundle);
        startActivity(myIntent);


    }

    public void configSocketEvents() {
        if (ConfigSocketIO.socket == null) {
            return;
        }
        ConfigSocketIO.socket.on("load recordings client", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                if (getActivity() == null) {
                    return;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];

                        Gson myGson = new Gson();
                        Type listType = new TypeToken<RecordingModelList>(){}.getType();
                        RecordingModelList recordingModel = myGson.fromJson(data.toString(), RecordingModelList.class);
                        recordingsList = recordingModel.recordings;
                        /*
                        try {
                            recordingsList.clear();
                            JSONArray jArray = (JSONArray)data.get("listRecordings");
                            if (jArray != null) {
                                for (int i=0;i<jArray.length();i++){
                                    recordingsList.add((RecordingModel)jArray.get(i));
                                }
                            }
                            */
                            RecordingArrayAdapter myAdapter = new RecordingArrayAdapter(getActivity().getApplicationContext(), R.layout.item_my_recording, recordingsList);
                            ListView lv = (ListView) getListView();
                            lv.setAdapter(myAdapter);
                        /*
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
*/

                    }
                });
            }
        });
    }


}