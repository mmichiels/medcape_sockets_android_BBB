package com.miau.gatos.typingbattleonline.flotandroidchart.flot.data;

public class SpecData {
	public double i0;
	public String i1;
	
	public SpecData(double i, String time) {
		this.i0 = i;
		this.i1 = time;
	}
}
