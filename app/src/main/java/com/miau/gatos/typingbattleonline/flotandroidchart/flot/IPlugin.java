package com.miau.gatos.typingbattleonline.flotandroidchart.flot;

public interface IPlugin {
	public void init(FlotDraw fd);
}
