package com.miau.gatos.typingbattleonline;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by gatos on 05/07/2016.
 */
public class RecordingArrayAdapter extends ArrayAdapter {
    private Context context;
    private ArrayList<RecordingModel> recordingsList;
    private int resource;
    private LayoutInflater inflater;

    public RecordingArrayAdapter(Context context, int resource, ArrayList<RecordingModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.recordingsList = objects;
        this.resource = resource;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if(convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(resource, null);
           //holder.playerImageItem = (ImageView)convertView.findViewById(R.id.playerImageItem);
            holder.date_recording_item = (TextView)convertView.findViewById(R.id.date_recording_item);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        //final ProgressBar progressBar = (ProgressBar)convertView.findViewById(R.id.progressBar);

        //ImageLoader.getInstance().displayImage(movieModelList.get(position).getImage(), holder.ivMovieIcon, new ImageLoadingListener() {

            //holder.playerImageItem.setImageResource(patientsList.get(position).getOpponentPlayerImage());

        holder.date_recording_item.setText(recordingsList.get(position).getDate());



        final Button buttonDeleteGame = (Button) convertView.findViewById(R.id.buttonDeleteGame);


        buttonDeleteGame.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                new AlertDialog.Builder(context)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Delete game")
                        .setMessage("Are you sure you want to delete this game?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                JSONObject data = new JSONObject();
                                try {
                                    data.put("player1Name", "");
                                    ConfigSocketIO.socket.emit("delete game", data);
                                } catch (JSONException e) {
                                    System.out.println(e);
                                }

                            }

                        })
                        .setNegativeButton("No", null)
                        .show();

            }
        });

        return convertView;
        }

    class ViewHolder{
        private TextView date_recording_item;
    }

}

