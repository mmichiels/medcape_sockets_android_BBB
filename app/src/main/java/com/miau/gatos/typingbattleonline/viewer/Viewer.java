package com.miau.gatos.typingbattleonline.viewer;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


import com.miau.gatos.typingbattleonline.ConfigSocketIO;
import com.miau.gatos.typingbattleonline.bluetooth.BluetoothConnection;
import com.miau.gatos.typingbattleonline.flotandroidchart.flot.FlotChartContainer;
import com.miau.gatos.typingbattleonline.flotandroidchart.flot.FlotDraw;
import com.miau.gatos.typingbattleonline.flotandroidchart.flot.Options;
import com.miau.gatos.typingbattleonline.flotandroidchart.flot.data.SeriesData;
import com.miau.gatos.typingbattleonline.osea.OSEAFactory;
import com.miau.gatos.typingbattleonline.osea.detection.QRSDetector2;
import com.miau.gatos.typingbattleonline.ringBuffer.RingBuffer;
import com.miau.gatos.typingbattleonline.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import java.util.Vector;

import io.socket.emitter.Emitter;


public class Viewer extends AppCompatActivity {
	volatile private boolean send_transmission_online = false;
	private String doctor_socket_id;

	private static Context context;

	private Thread workerThread;
	volatile private boolean stopWorker;
	volatile private boolean stopWorkerOnline;

	final private BluetoothConnection BTConnection = new BluetoothConnection();

	volatile private  byte[] recvBuff;
	volatile private int bytesRead;

	private Options options;
	private Vector<SeriesData> vSeriesData;
	private FlotChartContainer flotChartContainer;
	private FlotDraw flotDraw;

	private int indexPlotADC;
	private SeriesData seriesDataADC_1;
	private double[][] dataADC_1;
	private SeriesData seriesDataADC_2;
	private double[][] dataADC_2;
	private SeriesData seriesDataADC_3;
	private double[][] dataADC_3;
	private SeriesData seriesDataADC_4;
	private double[][] dataADC_4;
	private SeriesData seriesDataADC_5;
	private double[][] dataADC_5;
	private SeriesData seriesDataADC_6;
	private double[][] dataADC_6;
	private SeriesData seriesDataADC_7;
	private double[][] dataADC_7;
	private SeriesData seriesDataADC_8;
	private double[][] dataADC_8;

	private RingBuffer ringBuffer;

	private int nSample;

	private ToggleButton bluetoothButton;
	private Button synchButton;

	private String state;
	private Time now;
	private File rawFile;

	private static final int BUFFER_FILTER_SIZE = 5000;
	private ArrayList<Integer> buffer_filter;
	private static final int SAMPLE_RATE_HZ = 250;
	private static final int SAMPLE_RATE_MS = 1;
	private static final double RULE_TRANSLATE_PPM = (double)10 / 12;
	private TextView ppm_label;

	private static final int BUFFER_SEND_ONLINE = 15000;
	volatile private ArrayList<Byte> buffer_send_online;


	private BufferedOutputStream rawOutputStream;
	private FileWriter synchEventsFile;

	/*
	 * The tag has a 1 byte length (TAG = 0xB5)
	 * The ADC samples N-channels and each channel stores M-samples (uint16) so
	 * the size of the packet is 2*N*M, but we wrap half the buffer so 2*N*M/2
	 * The Nonin packet contains 6 bytes: 2 (Pleth) + 1 (SpO2) + 2 (Pleth) + 1 (SpO2)
	 */
	public static final byte PACKET_TAG            = (byte) 0xC0;
	public static final int ADC_CHANNELS           = 8;
	public static final int ADC_SIZE_CHANNEL       = 2;
	public static final int ADC_STATUS_BYTES       = 2; //Actually there are 3 status bytes, but 1 of them has been removed just to make pair the number of bytes
	public static final int ADC_PACKET_SIZE        = ADC_STATUS_BYTES + (ADC_CHANNELS*ADC_SIZE_CHANNEL);
	public static final int PACKET_SIZE            = ADC_PACKET_SIZE;
	public static final int MIN_RING_BUFFER_LENGTH = PACKET_SIZE + 1;

	/*
	 * Mode Nonin = {2, 7}
	 * Mode 2: 0x0 + 1 byte Pleth + 1 byte SpO2
	 * Mode 7: 1 byte Pleth (MSB) + 1 byte Pleth (LSB) + 1 byte SpO2
	 */


	@Override
	public void onResume() {
		super.onResume();
		configSocketEvents();
		//sendTransmissionOnline();
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_plot);
		context = this;
		ppm_label = (TextView) findViewById(R.id.ppm_label);


		options = new Options();
		options.fps = 50;
		options.yaxis.min = 0;
		options.yaxis.max = 800;

		dataADC_1 = new double[1500][2];
		for (int i = 0; i < dataADC_1.length; i++) {
			dataADC_1[i][0] = i;
			dataADC_1[i][1] = 0.0d;
		}

		dataADC_2 = new double[1500][2];
		for (int i = 0; i < dataADC_2.length; i++) {
			dataADC_2[i][0] = i;
			dataADC_2[i][1] = 0.0d;
		}

		dataADC_3 = new double[1500][2];
		for (int i = 0; i < dataADC_3.length; i++) {
			dataADC_3[i][0] = i;
			dataADC_3[i][1] = 0.0d;
		}

		dataADC_4 = new double[1500][2];
		for (int i = 0; i < dataADC_4.length; i++) {
			dataADC_4[i][0] = i;
			dataADC_4[i][1] = 0.0d;
		}

		dataADC_5 = new double[1500][2];
		for (int i = 0; i < dataADC_5.length; i++) {
			dataADC_5[i][0] = i;
			dataADC_5[i][1] = 0.0d;
		}

		dataADC_6 = new double[1500][2];
		for (int i = 0; i < dataADC_6.length; i++) {
			dataADC_6[i][0] = i;
			dataADC_6[i][1] = 0.0d;
		}

		dataADC_7 = new double[1500][2];
		for (int i = 0; i < dataADC_7.length; i++) {
			dataADC_7[i][0] = i;
			dataADC_7[i][1] = 0.0d;
		}

		dataADC_8 = new double[1500][2];
		for (int i = 0; i < dataADC_8.length; i++) {
			dataADC_8[i][0] = i;
			dataADC_8[i][1] = 0.0d;
		}

		seriesDataADC_1 = new SeriesData();
		seriesDataADC_1.setData(dataADC_1);
		seriesDataADC_1.label = "ADC 1 (señal interna)";
		seriesDataADC_1.series.color = 0x80000D;

		seriesDataADC_2 = new SeriesData();
		seriesDataADC_2.setData(dataADC_2);
		seriesDataADC_2.label = "ADC 2";
		seriesDataADC_2.series.color = 0x00800D;

		seriesDataADC_3 = new SeriesData();
		seriesDataADC_3.setData(dataADC_3);
		seriesDataADC_3.label = "ADC 3";
		seriesDataADC_3.series.color = 0x00400A;

		seriesDataADC_4 = new SeriesData();
		seriesDataADC_4.setData(dataADC_4);
		seriesDataADC_4.label = "ADC 4";
		seriesDataADC_4.series.color = 0x08100C;

		seriesDataADC_5 = new SeriesData();
		seriesDataADC_5.setData(dataADC_5);
		seriesDataADC_5.label = "ADC 5";
		seriesDataADC_5.series.color = 0x08100C;

		seriesDataADC_6 = new SeriesData();
		seriesDataADC_6.setData(dataADC_6);
		seriesDataADC_6.label = "ADC 6";
		seriesDataADC_6.series.color = 0x08100C;

		seriesDataADC_7 = new SeriesData();
		seriesDataADC_7.setData(dataADC_7);
		seriesDataADC_7.label = "ADC 7";
		seriesDataADC_7.series.color = 0x08100C;

		seriesDataADC_8 = new SeriesData();
		seriesDataADC_8.setData(dataADC_8);
		seriesDataADC_8.label = "ADC 8";
		seriesDataADC_8.series.color = 0x08100C;



		vSeriesData = new Vector<SeriesData>();
		vSeriesData.add(seriesDataADC_1);
		vSeriesData.add(seriesDataADC_2);
		vSeriesData.add(seriesDataADC_3);
		vSeriesData.add(seriesDataADC_4);
		vSeriesData.add(seriesDataADC_5);
		vSeriesData.add(seriesDataADC_6);
		vSeriesData.add(seriesDataADC_7);
		vSeriesData.add(seriesDataADC_8);

		flotDraw = new FlotDraw(vSeriesData, options, null);
		flotChartContainer = (FlotChartContainer) this.findViewById(R.id.flotDraw);
		if (flotChartContainer != null) {
			flotChartContainer.setDrawData(flotDraw);
		}

		ringBuffer = new RingBuffer();
		buffer_filter = new ArrayList<Integer>();
		buffer_send_online = new ArrayList<Byte>();

		state = Environment.getExternalStorageState();
		now = new Time();
		now.setToNow();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			File dirFile = new File(Environment.getExternalStorageDirectory() + "/BBB/");
			dirFile.mkdir();
			rawFile = new File(dirFile.toString() + "/" + now.format("%Y.%m.%d-%H.%M.%S") + "-raw.log");
			try {
				rawOutputStream = new BufferedOutputStream(new FileOutputStream(rawFile));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		nSample = 0;

		bluetoothButton = (ToggleButton) findViewById(R.id.BluetoothButton);

		// Button
		bluetoothButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					if (BTConnection.findDevice(Viewer.this)) {
						Toast.makeText(getApplicationContext(), "Bluetooth device found", Toast.LENGTH_SHORT).show();
						UUID uuid = UUID.fromString("34B1CF4D-1069-4AD6-89B6-E161D79BE4D8");
						if (BTConnection.open(uuid)) {
							Toast.makeText(getApplicationContext(), "Bluetooth device opened", Toast.LENGTH_LONG).show();
							beginListen();
						} else {
							bluetoothButton.setChecked(false);
							Toast.makeText(getApplicationContext(), "Bluetooth device not opened", Toast.LENGTH_LONG).show();
						}
					} else {
						bluetoothButton.setChecked(false);
						Toast.makeText(getApplicationContext(), "Bluetooth device not found", Toast.LENGTH_LONG).show();
					}
				} else {
					if (BTConnection.close()) {
						stopWorker = true;
						Toast.makeText(getApplicationContext(), "Bluetooth connection closed", Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(getApplicationContext(), "Bluetooth connection already closed", Toast.LENGTH_LONG).show();
					}
				}
			}
		});

		// Button: synchronization event
		synchButton = (Button) findViewById(R.id.synchButton);
		synchButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (synchEventsFile == null) {
					if (Environment.MEDIA_MOUNTED.equals(state)) {
						File dirFile = new File(Environment.getExternalStorageDirectory() + "/BBB/");
						dirFile.mkdir();
						try {
							synchEventsFile = new FileWriter(dirFile.toString() + "/" +
									now.format("%Y.%m.%d-%H.%M.%S") + "-synchoEvents.txt");
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				try {
					synchEventsFile.write("Evento de sincronizacion en muestra " + Integer.toString(nSample) + " de ADC\n");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				synchButton.setBackgroundColor(Color.RED);
				Toast.makeText(getApplicationContext(),
						"New synchro event introduced", Toast.LENGTH_SHORT).show();
			}
		});

		//sendTransmissionOnline();
	}


	public void sendTransmissionOnline(){
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (!Thread.currentThread().isInterrupted()) {
					if (send_transmission_online) {
						Log.w("", "Sending transmission online");

						byte[] recvBuff_send_socket = new byte[bytesRead];
						//Log.w("", "bytesRead:" + bytesRead);
						//Log.w("", "recvBuff length:" + recvBuff.length);

						for (int i = 0; i < bytesRead; i++) {
							if (i < recvBuff.length && i < recvBuff_send_socket.length) {
								recvBuff_send_socket[i] = recvBuff[i];
							}
						}
						JSONObject data = new JSONObject();
						try {
							data.put("doctor_socket_id", doctor_socket_id);
							data.put("transmission", recvBuff_send_socket);
							//Toast.makeText(context, "Transmission sent to doctor: " + doctor_socket_id, Toast.LENGTH_SHORT).show();

							ConfigSocketIO.socket.emit("emit transmission server", data);
							//stopWorkerOnline = true;
						} catch (JSONException e) {
							System.out.println(e);
						}
					}
				}
			}
		}).start();

	}

	void beginListen() {

		//final Handler handler = new Handler();

		stopWorker = false;

		workerThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (!Thread.currentThread().isInterrupted() && !stopWorker) {

					if (BTConnection.receiveDataIsAvailable()) {

						recvBuff = BTConnection.receiveData();
						bytesRead = BTConnection.getBytesRead();
						//int[] recvData = new int[bytesRead >> 1];

						if (send_transmission_online) {
							byte[] recvBuff_send_socket = new byte[bytesRead];
							for (int i = 0; i < bytesRead; i++) {
								if (i < recvBuff.length) {
									recvBuff_send_socket[i] = recvBuff[i];
								}
							}
							JSONObject data = new JSONObject();
							try {
								data.put("doctor_socket_id", doctor_socket_id);
								data.put("transmission", recvBuff_send_socket);
								//Toast.makeText(context, "Transmission sent to doctor: " + doctor_socket_id, Toast.LENGTH_SHORT).show();

								ConfigSocketIO.socket.emit("emit transmission server", data);
							} catch (JSONException e) {
								System.out.println(e);
							}
						}


						for (int i = 0; i < bytesRead; i++) {
							if (i < recvBuff.length){
								ringBuffer.add(recvBuff[i]);
							}
						}


					}

					while (ringBuffer.size() > MIN_RING_BUFFER_LENGTH) {
						double point;
						//BTConnection.sendData(1);
						byte byte_get = ringBuffer.get();
						switch (byte_get) {

							case PACKET_TAG:

								boolean corrupt_packet = false;
								add_send_samples_online(byte_get);

								byte[] bytes_packet = new byte[ADC_PACKET_SIZE - 1];
								for (int i = 0; i < ADC_PACKET_SIZE - 1; i++) {
									bytes_packet[i] = ringBuffer.get();
									add_send_samples_online(bytes_packet[i]);
									if (bytes_packet[i] == PACKET_TAG) {
										corrupt_packet = true;
										Log.w("", "Packet corrupt");
										break;
									}
								}
								if (!corrupt_packet) {
									int first_byte = bytes_packet[1];
									int second_byte = bytes_packet[2];
									point = (first_byte << 8 | second_byte) + 700;
									seriesDataADC_1.datapoints.points.set(2*indexPlotADC + 1, (double) point);


									int third_byte = bytes_packet[3] ;
									int fourth_byte = bytes_packet[4] ;
									point = (third_byte << 8 | fourth_byte) + 600;
									seriesDataADC_2.datapoints.points.set(2*indexPlotADC + 1, (double) point);


									int fifth_byte = bytes_packet[5];
									int sixth_byte = bytes_packet[6];
									point = (fifth_byte << 8 | sixth_byte) + 500;
									detect_qrs((int)Math.round((fifth_byte << 8 | sixth_byte)));
									seriesDataADC_3.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									int seventh_byte = bytes_packet[7] ;
									int eighth_byte = bytes_packet[8];
									point = (seventh_byte << 8 | eighth_byte) + 400;
									seriesDataADC_4.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									int byte_9 = bytes_packet[9] ;
									int byte_10 = bytes_packet[10];
									point = (byte_9 << 8 | byte_10) + 300;
									seriesDataADC_5.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									int byte_11 = bytes_packet[11] ;
									int byte_12 = bytes_packet[12];
									point = (byte_11 << 8 | byte_12) + 200;
									seriesDataADC_6.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									int byte_13 = bytes_packet[13] ;
									int byte_14 = bytes_packet[14];
									point = (byte_13 << 8 | byte_14) + 100;
									seriesDataADC_7.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									int byte_15 = bytes_packet[15] ;
									int byte_16 = bytes_packet[16];
									point = (byte_15 << 8 | byte_16) + 10;
									seriesDataADC_8.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									indexPlotADC++;

									if (indexPlotADC == dataADC_1.length || indexPlotADC == dataADC_2.length || indexPlotADC == dataADC_3.length || indexPlotADC == dataADC_4.length || indexPlotADC == dataADC_5.length || indexPlotADC == dataADC_6.length || indexPlotADC == dataADC_7.length || indexPlotADC == dataADC_8.length) {
										indexPlotADC = 0;
										//BTConnection.sendData(1);
									}





									nSample++;
									/*
									for (int j=10; j < ADC_PACKET_SIZE; j++) { //We don't need these channels (for now we only print channel 1)
										ringBuffer.get();
									}
									*/
									//send_transmission_online = true;

								}
								break;

							default:
								add_send_samples_online(byte_get);

								Log.w("", "Tag missed");
								break;
						}
					}
				}
			}
		});

		workerThread.start();

	}
	/*
	public void sendTransmissionOnline() {
		byte[] recvBuff_send_socket = new byte[bytesRead];
		for (int i = 0; i < bytesRead; i++) {
			if (i < recvBuff.length) {
				recvBuff_send_socket[i] = recvBuff[i];
			}
		}
		JSONObject data = new JSONObject();
		try {
			data.put("doctor_socket_id", doctor_socket_id);
			data.put("transmission", recvBuff_send_socket);
			//Toast.makeText(context, "Transmission sent to doctor: " + doctor_socket_id, Toast.LENGTH_SHORT).show();

			ConfigSocketIO.socket.emit("emit transmission server", data);
		} catch (JSONException e) {
			System.out.println(e);
		}
	}
	*/
	@Override
	public void onStop() {
		super.onStop();
		if (synchEventsFile != null) {
			try {
				synchEventsFile.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (rawOutputStream != null) {
			try {
				rawOutputStream.flush();
				rawOutputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (BTConnection.close()) {
			stopWorker = true;
		}
	}


	public void add_send_samples_online(byte sample) {
		if (buffer_send_online.size() < BUFFER_SEND_ONLINE - 1) {
			buffer_send_online.add(sample);
		} else {
				Log.w("", "Sending transmission online...");

				byte[] recvBuff_send_socket = new byte[buffer_send_online.size()];
				for (int i = 0; i < buffer_send_online.size(); i++) {
						recvBuff_send_socket[i] = buffer_send_online.get(i);
				}
				JSONObject data = new JSONObject();
				try {
					data.put("transmission", recvBuff_send_socket);
					//Toast.makeText(context, "Transmission sent to doctor: " + doctor_socket_id, Toast.LENGTH_SHORT).show();

					ConfigSocketIO.socket.emit("upload transmission server", data);
				} catch (JSONException e) {
					System.out.println(e);
				}

			buffer_send_online.clear();
			buffer_send_online.add(sample);
		}
	}

	public void detect_qrs(int sample){
		if (buffer_filter.size() < BUFFER_FILTER_SIZE-1) {
			buffer_filter.add(sample);
		} else {
			Log.w("", "Starting qrs detection...");

			QRSDetector2 qrsDetector = OSEAFactory.createQRSDetector2(SAMPLE_RATE_HZ);
			int counter_qrs = 0;
			for (int i = 0; i < buffer_filter.size(); i++) {
				//Log.w("", "Sample is " + buffer_filter.get(i));

				int result = qrsDetector.QRSDet(buffer_filter.get(i));
				if (result != 0) {
					counter_qrs += 1;
					Log.w("", "A QRS-Complex was detected at sample: " + (i-result));
				}
			}

			int miliseconds_elapsed = SAMPLE_RATE_MS * BUFFER_FILTER_SIZE;
			/*
			int seconds_elapsed = miliseconds_elapsed / 1000;
			int minutes_elapsed = seconds_elapsed / 60;
			*/
			Log.e("", "counter_qrs: " + counter_qrs);

			final long ppm_not_rule =  Math.round(60000*counter_qrs / (double)miliseconds_elapsed);
			Log.e("", "ppm_not_rule: " + ppm_not_rule);

			final long ppm = Math.round((double)ppm_not_rule*RULE_TRANSLATE_PPM);
			Log.e("", "ppm: " + ppm);
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					ppm_label.setText(ppm + " ppm");
				}
			});


			JSONObject data = new JSONObject();
			try {
				data.put("doctor_socket_id", doctor_socket_id);
				data.put("ppm", ppm);
				//Toast.makeText(context, "Transmission sent to doctor: " + doctor_socket_id, Toast.LENGTH_SHORT).show();

				ConfigSocketIO.socket.emit("emit ppm server", data);
			} catch (JSONException e) {
				System.out.println(e);
			}

			//buffer_filter.remove(buffer_filter.size() - 1);
			buffer_filter.clear();
			buffer_filter.add(sample);
		}

	}

	/*
	public static int[] detect(float[] ecg) {
		// circular buffer for input ecg signal
		// we need to keep a history of M + 1 samples for HP filter
		float[] ecg_circ_buff = new float[M + 1];
		int ecg_circ_WR_idx = 0;
		int ecg_circ_RD_idx = 0;

		// circular buffer for input ecg signal
		// we need to keep a history of N+1 samples for LP filter
		float[] hp_circ_buff = new float[N+1];
		int hp_circ_WR_idx = 0;
		int hp_circ_RD_idx = 0;

		// LP filter outputs a single point for every input point
		// This goes straight to adaptive filtering for eval
		float next_eval_pt = 0;

		// output
		int[] QRS = new int[ecg.length];

		// running sums for HP and LP filters, values shifted in FILO
		float hp_sum = 0;
		float lp_sum = 0;

		// parameters for adaptive thresholding
		double treshold = 0;
		boolean triggered = false;
		int trig_time = 0;
		float win_max = 0;
		int win_idx = 0;

		for(int i = 0; i < ecg.length; i++){
			ecg_circ_buff[ecg_circ_WR_idx++] = ecg[i];
			ecg_circ_WR_idx %= (M+1);

				// High pass filtering
			if(i < M){
				// first fill buffer with enough points for HP filter
				hp_sum += ecg_circ_buff[ecg_circ_RD_idx];
				hp_circ_buff[hp_circ_WR_idx] = 0;
			}
			else{
				hp_sum += ecg_circ_buff[ecg_circ_RD_idx];

				int tmp = ecg_circ_RD_idx - M;
				if(tmp < 0){
					tmp += M + 1;
				}
				hp_sum -= ecg_circ_buff[tmp];

				float y1 = 0;
				float y2 = 0;

				tmp = (ecg_circ_RD_idx - ((M+1)/2));
				if(tmp < 0){
					tmp += M + 1;
				}
				y2 = ecg_circ_buff[tmp];

				y1 = HP_CONSTANT * hp_sum;

				hp_circ_buff[hp_circ_WR_idx] = y2 - y1;
			}

			ecg_circ_RD_idx++;
			ecg_circ_RD_idx %= (M+1);

			hp_circ_WR_idx++;
			hp_circ_WR_idx %= (N+1);

				// Low pass filtering

			// shift in new sample from high pass filter
			lp_sum += hp_circ_buff[hp_circ_RD_idx] * hp_circ_buff[hp_circ_RD_idx];

			if(i < N){
				// first fill buffer with enough points for LP filter
				next_eval_pt = 0;

			}
			else{
				// shift out oldest data point
				int tmp = hp_circ_RD_idx - N;
				if(tmp < 0){
					tmp += N+1;
				}
				lp_sum -= hp_circ_buff[tmp] * hp_circ_buff[tmp];

				next_eval_pt = lp_sum;
			}

			hp_circ_RD_idx++;
			hp_circ_RD_idx %= (N+1);

				// Adapative thresholding beat detection
			// set initial threshold
			if(i < winSize) {
				if(next_eval_pt > treshold) {
					treshold = next_eval_pt;
				}
			}

			// check if detection hold off period has passed
			if(triggered){
				trig_time++;

				if(trig_time >= 100){
					triggered = false;
					trig_time = 0;
				}
			}

			// find if we have a new max
			if(next_eval_pt > win_max) win_max = next_eval_pt;

			// find if we are above adaptive threshold
			if(next_eval_pt > treshold && !triggered) {
				QRS[i] = 1;

				triggered = true;
			}
			else {
				QRS[i] = 0;
			}

			// adjust adaptive threshold using max of signal found
			// in previous window
			if(++win_idx > winSize){
				// weighting factor for determining the contribution of
				// the current peak value to the threshold adjustment
				double gamma = 0.175;

				// forgetting factor -
				// rate at which we forget old observations
				double alpha = 0.01 + (Math.random() * ((0.1 - 0.01)));

				treshold = alpha * gamma * win_max + (1 - alpha) * treshold;

				// reset current window ind
				win_idx = 0;
				win_max = -10000000;
			}
		}

		return QRS;
	}
	*/
	public void configSocketEvents() {

		ConfigSocketIO.socket.on("request transmission client", new Emitter.Listener() {
			@Override
			public void call(final Object... args) {
				if (this == null) {
					return;
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						send_transmission_online = true;
						JSONObject data = (JSONObject) args[0];
						try {
							doctor_socket_id = data.getString("doctor_socket_id");
							Toast.makeText(context, "Transmission accepted from doctor" + doctor_socket_id, Toast.LENGTH_SHORT).show();
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				});
			}
		});

	}
}
