package com.miau.gatos.typingbattleonline;

import java.io.Serializable;

/**
 * Created by gatos on 05/07/2016.
 */
public class IncidenceModel implements Serializable {

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getType_incidence() {
        return type_incidence;
    }

    public void setType_incidence(String type_incidence) {
        this.type_incidence = type_incidence;
    }

    public int getValue_incidence() {
        return value_incidence;
    }

    public void setValue_incidence(int value_incidence) {
        this.value_incidence = value_incidence;
    }

    public String getDate_incidence() {
        return date_incidence;
    }

    public void setDate_incidence(String date_incidence) {
        this.date_incidence = date_incidence;
    }

    private String _id;
    private String type_incidence;
    private int value_incidence;
    private String date_incidence;



}
