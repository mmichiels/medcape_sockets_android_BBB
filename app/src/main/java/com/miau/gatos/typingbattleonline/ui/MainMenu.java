package com.miau.gatos.typingbattleonline.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.miau.gatos.typingbattleonline.ConfigSocketIO;
import com.miau.gatos.typingbattleonline.R;
import com.miau.gatos.typingbattleonline.viewer.Viewer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainMenu extends AppCompatActivity {
    private static String userLogged;
    private static Context context;

    private TextView label_user_logged;
    //Socket.io-------------
    private Socket socket;
    //-------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        context = this;

        label_user_logged = (TextView) findViewById(R.id.label_user_logged);

        Bundle b = getIntent().getExtras(); //Para coger el nivel que se quiera jugar
        if (b != null) {
            this.userLogged = b.getString("userLogged").trim();
            Toast.makeText(this, "Bienvenido " + this.userLogged, Toast.LENGTH_SHORT).show();

            //final EditText label_user_logged_text = (EditText)  findViewById(R.id.label_user_logged);
            //String text_label_user_logged = label_user_logged_text.getText().toString().trim();
            label_user_logged.setText("Welcome back: " + this.userLogged);
        } else {
            Toast.makeText(this, "Error al obtener el nombre del usuario", Toast.LENGTH_SHORT).show();
        }


        Button multiplayerButton = (Button) findViewById(R.id.multiplayerButton);
        Button singlePlayerButton = (Button) findViewById(R.id.singlePlayerButton);
        Button my_profile_Button = (Button) findViewById(R.id.my_profile_Button);

        singlePlayerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(MainMenu.this, Viewer.class);
                startActivity(myIntent);
            }
        });


        multiplayerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(MainMenu.this, MultiplayerMenu.class);
                startActivity(myIntent);
            }
        });

        my_profile_Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(MainMenu.this, ProfilePatient.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("patient_name", userLogged);
                myIntent.putExtras(bundle);
                startActivity(myIntent);
            }
        });


        //Socket.io-------------
        connectSocket();
        configSocketEvents();
        ConfigSocketIO.socket = this.socket;
        ConfigSocketIO.user = this.userLogged;
        //-------------------------
    }


    public void connectSocket() {
        try {
            socket = IO.socket(ConfigSocketIO.serverURL);

            socket.connect();
            JSONObject data = new JSONObject();
            try {
                data.put("name", userLogged);
                socket.emit("new user", data);
            } catch (JSONException e) {
                System.out.println(e);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }


    public void configSocketEvents() {
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                if (this == null) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                            Toast.makeText(context, "conectado al socket", Toast.LENGTH_SHORT).show();
                        }
                });
            }
        }).on("new user client", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                if (this == null) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            String state = data.getString("state");
                            Toast.makeText(context, "datos cargados", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}
