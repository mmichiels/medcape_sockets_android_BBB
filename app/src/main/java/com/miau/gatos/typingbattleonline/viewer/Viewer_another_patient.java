package com.miau.gatos.typingbattleonline.viewer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.miau.gatos.typingbattleonline.ConfigSocketIO;
import com.miau.gatos.typingbattleonline.R;
import com.miau.gatos.typingbattleonline.bluetooth.BluetoothConnection;
import com.miau.gatos.typingbattleonline.flotandroidchart.flot.FlotChartContainer;
import com.miau.gatos.typingbattleonline.flotandroidchart.flot.FlotDraw;
import com.miau.gatos.typingbattleonline.flotandroidchart.flot.Options;
import com.miau.gatos.typingbattleonline.flotandroidchart.flot.data.SeriesData;
import com.miau.gatos.typingbattleonline.ringBuffer.RingBuffer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;
import java.util.Vector;

import io.socket.emitter.Emitter;


public class Viewer_another_patient extends AppCompatActivity {
	private static Context context;

	private Thread workerThread;
	volatile private boolean stopWorker;

	final private BluetoothConnection BTConnection = new BluetoothConnection();

	private Options options;
	private Vector<SeriesData> vSeriesData;
	private FlotChartContainer flotChartContainer;
	private FlotDraw flotDraw;

	private int indexPlotADC;
	private SeriesData seriesDataADC_1;
	private double[][] dataADC_1;
	private SeriesData seriesDataADC_2;
	private double[][] dataADC_2;
	private SeriesData seriesDataADC_3;
	private double[][] dataADC_3;
	private SeriesData seriesDataADC_4;
	private double[][] dataADC_4;
	private SeriesData seriesDataADC_5;
	private double[][] dataADC_5;
	private SeriesData seriesDataADC_6;
	private double[][] dataADC_6;
	private SeriesData seriesDataADC_7;
	private double[][] dataADC_7;
	private SeriesData seriesDataADC_8;
	private double[][] dataADC_8;

	private int indexPlotUARTPleth;
	private SeriesData seriesDataUARTPleth;
	private double[][] dataUARTPleth;

	private RingBuffer ringBuffer;

	private int nSample;

	private Button synchButton;

	private String state;
	private Time now;
	private File rawFile;
	private BufferedOutputStream rawOutputStream;
	private FileWriter synchEventsFile;

	/*
	 * The tag has a 1 byte length (TAG = 0xB5)
	 * The ADC samples N-channels and each channel stores M-samples (uint16) so
	 * the size of the packet is 2*N*M, but we wrap half the buffer so 2*N*M/2
	 * The Nonin packet contains 6 bytes: 2 (Pleth) + 1 (SpO2) + 2 (Pleth) + 1 (SpO2)
	 */
	public static final byte PACKET_TAG            = (byte) 0xC0;
	public static final int ADC_CHANNELS           = 8;
	public static final int ADC_SIZE_CHANNEL       = 2;
	public static final int ADC_STATUS_BYTES       = 2; //Actually there are 3 status bytes, but 1 of them has been removed just to make pair the number of bytes
	public static final int ADC_PACKET_SIZE        = ADC_STATUS_BYTES + (ADC_CHANNELS*ADC_SIZE_CHANNEL);
	public static final int PACKET_SIZE            = ADC_PACKET_SIZE;
	public static final int MIN_RING_BUFFER_LENGTH = PACKET_SIZE + 1;

	/*
	 * Mode Nonin = {2, 7}
	 * Mode 2: 0x0 + 1 byte Pleth + 1 byte SpO2
	 * Mode 7: 1 byte Pleth (MSB) + 1 byte Pleth (LSB) + 1 byte SpO2
	 */

	@Override
	public void onResume() {
		super.onResume();
		configSocketEvents();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_plot_another_patient);
		context = this;
		//ppm_label = (TextView) findViewById(R.id.ppm_label);


		options = new Options();
		options.fps = 50;
		options.yaxis.min = 0;
		options.yaxis.max = 800;

		dataADC_1 = new double[1500][2];
		for (int i = 0; i < dataADC_1.length; i++) {
			dataADC_1[i][0] = i;
			dataADC_1[i][1] = 0.0d;
		}

		dataADC_2 = new double[1500][2];
		for (int i = 0; i < dataADC_2.length; i++) {
			dataADC_2[i][0] = i;
			dataADC_2[i][1] = 0.0d;
		}

		dataADC_3 = new double[1500][2];
		for (int i = 0; i < dataADC_3.length; i++) {
			dataADC_3[i][0] = i;
			dataADC_3[i][1] = 0.0d;
		}

		dataADC_4 = new double[1500][2];
		for (int i = 0; i < dataADC_4.length; i++) {
			dataADC_4[i][0] = i;
			dataADC_4[i][1] = 0.0d;
		}

		dataADC_5 = new double[1500][2];
		for (int i = 0; i < dataADC_5.length; i++) {
			dataADC_5[i][0] = i;
			dataADC_5[i][1] = 0.0d;
		}

		dataADC_6 = new double[1500][2];
		for (int i = 0; i < dataADC_6.length; i++) {
			dataADC_6[i][0] = i;
			dataADC_6[i][1] = 0.0d;
		}

		dataADC_7 = new double[1500][2];
		for (int i = 0; i < dataADC_7.length; i++) {
			dataADC_7[i][0] = i;
			dataADC_7[i][1] = 0.0d;
		}

		dataADC_8 = new double[1500][2];
		for (int i = 0; i < dataADC_8.length; i++) {
			dataADC_8[i][0] = i;
			dataADC_8[i][1] = 0.0d;
		}

		seriesDataADC_1 = new SeriesData();
		seriesDataADC_1.setData(dataADC_1);
		seriesDataADC_1.label = "ADC 1 (señal interna)";
		seriesDataADC_1.series.color = 0x80000D;

		seriesDataADC_2 = new SeriesData();
		seriesDataADC_2.setData(dataADC_2);
		seriesDataADC_2.label = "ADC 2";
		seriesDataADC_2.series.color = 0x00800D;

		seriesDataADC_3 = new SeriesData();
		seriesDataADC_3.setData(dataADC_3);
		seriesDataADC_3.label = "ADC 3";
		seriesDataADC_3.series.color = 0x00400A;

		seriesDataADC_4 = new SeriesData();
		seriesDataADC_4.setData(dataADC_4);
		seriesDataADC_4.label = "ADC 4";
		seriesDataADC_4.series.color = 0x08100C;

		seriesDataADC_5 = new SeriesData();
		seriesDataADC_5.setData(dataADC_5);
		seriesDataADC_5.label = "ADC 5";
		seriesDataADC_5.series.color = 0x08100C;

		seriesDataADC_6 = new SeriesData();
		seriesDataADC_6.setData(dataADC_6);
		seriesDataADC_6.label = "ADC 6";
		seriesDataADC_6.series.color = 0x08100C;

		seriesDataADC_7 = new SeriesData();
		seriesDataADC_7.setData(dataADC_7);
		seriesDataADC_7.label = "ADC 7";
		seriesDataADC_7.series.color = 0x08100C;

		seriesDataADC_8 = new SeriesData();
		seriesDataADC_8.setData(dataADC_8);
		seriesDataADC_8.label = "ADC 8";
		seriesDataADC_8.series.color = 0x08100C;



		vSeriesData = new Vector<SeriesData>();
		vSeriesData.add(seriesDataADC_1);
		vSeriesData.add(seriesDataADC_2);
		vSeriesData.add(seriesDataADC_3);
		vSeriesData.add(seriesDataADC_4);
		vSeriesData.add(seriesDataADC_5);
		vSeriesData.add(seriesDataADC_6);
		vSeriesData.add(seriesDataADC_7);
		vSeriesData.add(seriesDataADC_8);

		flotDraw = new FlotDraw(vSeriesData, options, null);
		flotChartContainer = (FlotChartContainer) this.findViewById(R.id.flotDraw);
		if (flotChartContainer != null) {
			flotChartContainer.setDrawData(flotDraw);
		}

		ringBuffer = new RingBuffer();

		state = Environment.getExternalStorageState();
		now = new Time();
		now.setToNow();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			File dirFile = new File(Environment.getExternalStorageDirectory() + "/BBB/");
			dirFile.mkdir();
			rawFile = new File(dirFile.toString() + "/" + now.format("%Y.%m.%d-%H.%M.%S") + "-raw.log");
			try {
				rawOutputStream = new BufferedOutputStream(new FileOutputStream(rawFile));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		nSample = 0;



		// Button: synchronization event
		synchButton = (Button) findViewById(R.id.synchButton);
		synchButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (synchEventsFile == null) {
					if (Environment.MEDIA_MOUNTED.equals(state)) {
						File dirFile = new File(Environment.getExternalStorageDirectory() + "/BBB/");
						dirFile.mkdir();
						try {
							synchEventsFile = new FileWriter(dirFile.toString() + "/" +
									now.format("%Y.%m.%d-%H.%M.%S") + "-synchoEvents.txt");
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				try {
					synchEventsFile.write("Evento de sincronizacion en muestra " + Integer.toString(nSample) + " de ADC\n");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				synchButton.setBackgroundColor(Color.RED);
				Toast.makeText(getApplicationContext(),
						"New synchro event introduced", Toast.LENGTH_SHORT).show();
			}
		});

		configSocketEvents();
	}

	void beginListen() {

		//final Handler handler = new Handler();

		stopWorker = false;
		/*
		workerThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (!Thread.currentThread().isInterrupted() && !stopWorker) {

					//El ringBuffer se rellena en el On de socket.io

					//while (ringBuffer.size() > MIN_RING_BUFFER_LENGTH) {

					//}

				}
			}
		});

		workerThread.start();
		*/
	}

	@Override
	public void onStop() {
		super.onStop();
		if (synchEventsFile != null) {
			try {
				synchEventsFile.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (rawOutputStream != null) {
			try {
				rawOutputStream.flush();
				rawOutputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (BTConnection.close()) {
			stopWorker = true;
		}
	}

	public void configSocketEvents() {
/*
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (!Thread.currentThread().isInterrupted()) {
					Log.w("", "Thread running");
*/
		ConfigSocketIO.socket.on("receive transmission client", new Emitter.Listener() {
			@Override
			public void call(final Object... args) {
				if (this == null) {
					return;
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						//Toast.makeText(context, "Transmission received from patient", Toast.LENGTH_SHORT).show();

						//System.out.println("Chunk received");
						JSONObject data = (JSONObject) args[0];
						byte[] recvBuff_received = new byte[0];
						try {
							recvBuff_received = (byte[])data.get("transmission");

						} catch (JSONException e) {
							e.printStackTrace();
						}

						for  (byte b : recvBuff_received) {
							ringBuffer.add(b);
						}

						double point;
						//BTConnection.sendData(1);

						switch (ringBuffer.get()) {

							case PACKET_TAG:
								boolean corrupt_packet = false;

								byte[] bytes_packet = new byte[ADC_PACKET_SIZE - 1];
								for (int i = 0; i < ADC_PACKET_SIZE - 1; i++) {
									bytes_packet[i] = ringBuffer.get();
									if (bytes_packet[i] == PACKET_TAG) {
										corrupt_packet = true;
										Log.w("", "Packet corrupt");
										break;
									}
								}
								if (!corrupt_packet) {
									int first_byte = bytes_packet[1];
									int second_byte = bytes_packet[2];
									point = (first_byte | second_byte) + 700;
									seriesDataADC_1.datapoints.points.set(2*indexPlotADC + 1, (double) point);


									int third_byte = bytes_packet[3] ;
									int fourth_byte = bytes_packet[4] ;
									point = (third_byte | fourth_byte) + 600;
									seriesDataADC_2.datapoints.points.set(2*indexPlotADC + 1, (double) point);


									int fifth_byte = bytes_packet[5];
									int sixth_byte = bytes_packet[6];
									point = (fifth_byte << 8 | sixth_byte) + 500;
									seriesDataADC_3.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									int seventh_byte = bytes_packet[7] ;
									int eighth_byte = bytes_packet[8];
									point = (seventh_byte | eighth_byte) + 400;
									seriesDataADC_4.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									int byte_9 = bytes_packet[9] ;
									int byte_10 = bytes_packet[10];
									point = (byte_9 | byte_10) + 300;
									seriesDataADC_5.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									int byte_11 = bytes_packet[11] ;
									int byte_12 = bytes_packet[12];
									point = (byte_11 | byte_12) + 200;
									seriesDataADC_6.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									int byte_13 = bytes_packet[13] ;
									int byte_14 = bytes_packet[14];
									point = (byte_13 | byte_14) + 100;
									seriesDataADC_7.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									int byte_15 = bytes_packet[15] ;
									int byte_16 = bytes_packet[16];
									point = (byte_15 | byte_16) + 0;
									seriesDataADC_8.datapoints.points.set(2*indexPlotADC + 1, (double) point);

									indexPlotADC++;

									if (indexPlotADC == dataADC_1.length || indexPlotADC == dataADC_2.length || indexPlotADC == dataADC_3.length || indexPlotADC == dataADC_4.length || indexPlotADC == dataADC_5.length || indexPlotADC == dataADC_6.length || indexPlotADC == dataADC_7.length || indexPlotADC == dataADC_8.length) {
										indexPlotADC = 0;
										//BTConnection.sendData(1);
									}

									nSample++;
									/*
									for (int j=10; j < ADC_PACKET_SIZE; j++) { //We don't need these channels (for now we only print channel 1)
										ringBuffer.get();
									}
									*/
								}
								break;

							default:
								Log.w("", "Tag missed");
								break;

						}
						/*
						try {
							if (recvBuff_received.length > 0) {
								rawOutputStream.write(recvBuff_received, 0, recvBuff_received.length);
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						*/
					}
				});
			}
		});

		ConfigSocketIO.socket.on("receive ppm client", new Emitter.Listener() {
			@Override
			public void call(final Object... args) {
				if (this == null) {
					Log.w("", "NULL receive ppm client received");
					return;
				}

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Log.w("", "OK receive ppm client received");

						JSONObject data = (JSONObject) args[0];
						long ppm;
						String message_alert;
						String patient_name;
						try {
							ppm = (int)data.get("ppm");
							patient_name = (String)data.get("patient_name");
							message_alert = (String)data.get("message_alert");

							// build notification
							// the addAction re-use the same intent to keep the example short
							Intent notificationIntent = new Intent(context, Viewer_another_patient.class);

							notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
							PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);


							NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
							mBuilder.setSmallIcon(R.drawable.logo_medcape);
							mBuilder.setContentTitle("Medcape");
							mBuilder.setContentText(message_alert + ". " + ppm + " PPM");
							mBuilder.setContentIntent(intent);
							Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
							mBuilder.setSound(alarmSound);

							NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

							// notificationID allows you to update the notification later on.
							mNotificationManager.notify(0, mBuilder.build());

						} catch (JSONException e) {
							e.printStackTrace();
						}

					}
				});




			}
		});

/*
				}
			}
		}).start();
*/
	}

}
