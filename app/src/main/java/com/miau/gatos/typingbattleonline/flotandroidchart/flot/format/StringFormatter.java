package com.miau.gatos.typingbattleonline.flotandroidchart.flot.format;

import com.miau.gatos.typingbattleonline.flotandroidchart.flot.data.SeriesData;

public interface StringFormatter {
	public String format(String str, SeriesData axis);

	public String formatNumber(double val, SeriesData axis);
}
