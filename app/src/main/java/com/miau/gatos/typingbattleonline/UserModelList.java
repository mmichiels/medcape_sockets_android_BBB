package com.miau.gatos.typingbattleonline;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gatos on 05/07/2016.
 */
public class UserModelList {
    @SerializedName("listUsers")
    public List<UserModel> patients;
}
