var router = require('./router');
express = require('express');

module.exports = function(){
	var app = express();
	router(app);
	return app;
};