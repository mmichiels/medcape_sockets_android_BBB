'use strict';

var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var UserSchema = new Schema({
	id: Number,
	name: String,
	language: String,
	country: String,
	patients: [String]
});

mongoose.model('User', UserSchema);