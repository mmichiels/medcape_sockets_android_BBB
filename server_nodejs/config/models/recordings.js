'use strict';

var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var RecordingsSchema = new Schema({
	id: Number,
	patient_id: String,
	patient_name: String,
	date: Date,
	recording: String,
	incidences: [{
		id_incidence: String,
		type_incidence: String,
		value_incidence: Number,
		date_incidence: Date
	}]
});

mongoose.model('Recordings', RecordingsSchema);