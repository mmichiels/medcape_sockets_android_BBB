var config = require('./env/development');
mongoose = require('mongoose');			
require('./models/user');
require('./models/recordings');

module.exports = function(){
	var db = mongoose.connect(config.db, function(err){
		if(err) {
			console.log("Imposible conectar a MongoDB");
			console.log(config.db);
		}
		else {

			console.log("Conectado a MongoDB");
		}
	});
	
	
	return db;
};