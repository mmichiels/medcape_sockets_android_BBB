var express = require('./config/express');
var mongoose = require('./config/mongoose');
var app = express();
var db = mongoose();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var fs = require('fs');

var userModel = db.model('User');
var recordingModel = db.model('Recordings');

var users = []; 
var connections = [];

server.listen(process.env.port || 3000);
console.log('Server running...');


io.sockets.on('connection', function(socket){
	connections.push(socket);
	console.log('Connected: %s sockets connected', connections.length);


	//Disconect
	socket.on('disconnect', function(data){
		//updateUsernames();

		connections.splice(connections.indexOf(socket), 1);
		console.log('Disconnected: %s sockets connected', connections.length);
	});

	//New user
	socket.on('new user', function(data, callback){
		console.log(data.name + ' connected. With socket id: ' + socket.id);
		users[data.name] = socket.id;
		var newUser = userModel.findOneAndUpdate(
			{name: data.name},
			{},
			{
				new:true,
				upsert:true
			},
			function(err) {
					if(err) return handleError(err);
			}

		);

		socket.emit("new user client", {state: "tus datos han sido cargados de MongoDB"});
		//callback(true);
	})

	socket.on('search player', function(data){
		console.log('Searching for player %s ', data.playerName);
		var playerNameFound;

		userModel.findOne(
			{'name': data.playerName}, function(err, playerFound){
				if(err) return handleError(err);
				if(playerFound === null) {
					playerNameFound = "not found";
				} else {
					playerNameFound = playerFound.name;
				}
				console.log('PERSON found is %s ', playerNameFound);
				socket.emit("player found client", {playerName: playerNameFound});
			}
		);

	});


	socket.on('load game server', function(data){
		console.log('Starting game vs ', data.opponentName);
		if(!data.winner){
			data.winner = "No winner";
		}

		gameModel.findOne(
			{ 	$or: [ {player1Name: data.opponentName, player2Name: data.myName}, 
				{player1Name: data.myName, player2Name: data.opponentName} ]
			},
			{
			},
			{
				new:true,
				upsert:false
			},
				function(err, gameFound) {
					if(err) return handleError(err);
					console.log('Creating new game vs ', data.opponentName);
					if(gameFound === null) {
						var phrasesData = ["a", "b"];
						var newGame = new gameModel(
						{
							numberOfPhrases: 2,
							player1Name: data.opponentName,
							player2Name: data.myName,
							phrases: phrasesData,
							turn: data.turn,
							gamesWonPlayer1: 0,
							gamesWonPlayer2: 0
						});

						newGame.save(function(err, newGame) {
								if(err) return handleError(err);
								socket.emit("load new game client", {gameFound: newGame});
							}
						);
					} else {
						socket.emit("load game client", {gameFound: gameFound});
					}
				}

		);
	});


	socket.on('send result server', function(data){
		console.log('Sending result to bbdd');
		data.scoresPlayer1 = JSON.parse(data.scoresPlayer1);
		data.scoresPlayer2 = JSON.parse(data.scoresPlayer2);
		

		gameModel.findOneAndUpdate(
			{ 	$or: [ {player1Name: data.opponentName, player2Name: data.myName}, 
				{player1Name: data.myName, player2Name: data.opponentName} ]
			},
			{
				turn: data.turn,
				scoresPlayer1: data.scoresPlayer1,
				scoresPlayer2: data.scoresPlayer2
			},
			{
				new:true,
				upsert:false
			},
			function(err, gameUpdated) {
				if(err) return handleError(err);
			}

		);

	});

		socket.on('send gameFinished server', function(data){
		console.log('Sending result to bbdd');
		

		gameModel.findOneAndUpdate(
			{ 	$or: [ {player1Name: data.opponentName, player2Name: data.myName}, 
				{player1Name: data.myName, player2Name: data.opponentName} ]
			},
			{
				turn: data.turn,
				gamesWonPlayer1: data.gamesWonPlayer1,
				gamesWonPlayer2: data.gamesWonPlayer2
			},
			{
				new:true,
				upsert:false
			},
			function(err, gameUpdated) {
				if(err) return handleError(err);
			}

		);

	});

	socket.on('rematch game server', function(data){
		console.log('Starting rematch');
		data.scoresPlayer1 = JSON.parse(data.scoresPlayer1);
		data.scoresPlayer2 = JSON.parse(data.scoresPlayer2);
		data.scoresPlayer1Previous = JSON.parse(data.scoresPlayer1Previous);
		data.scoresPlayer2Previous = JSON.parse(data.scoresPlayer2Previous);
		data.phrasesPrevious = data.phrasesPrevious.replace(/\[/g, ''); 
		data.phrasesPrevious = data.phrasesPrevious.replace(/\]/g, ''); 
		data.phrasesPrevious = data.phrasesPrevious.split(",");
		



		gameModel.findOneAndUpdate(
			{ 	$or: [ {player1Name: data.opponentName, player2Name: data.myName}, 
				{player1Name: data.myName, player2Name: data.opponentName} ]
			},
			{
				turn: data.turn,
				turnPrevious: data.turnPrevious,
				scoresPlayer1: data.scoresPlayer1,
				scoresPlayer1Previous: data.scoresPlayer1Previous,
				scoresPlayer2: data.scoresPlayer2,
				scoresPlayer2Previous: data.scoresPlayer2Previous,
				phrasesPrevious: data.phrasesPrevious
			},
			{
				new:true,
				upsert:false
			},
			function(err, gameUpdated) {
				if(err) return handleError(err);
			}

		);
	});


	socket.on('load games server', function(data){
		console.log('Loading games');
		/*
		gameModel.find(
			{$or: [ {player1Name: data.name}, 
				{player2Name: data.name} ]},
			function(err, gamesFound){
				if(err) return handleError(err);
				if(gamesFound === null) {
					gamesFound = "El jugador " + data.name + " no tiene partidas";
				} 
				socket.emit("load games client", {listGames: gamesFound});
			}
		);
		*/
	});
	
	socket.on('add patient to doctor server', function(data){
		var patient_socket_id = users[data.patient_name];
		
		var users_keys = Object.keys(users);
		var user_values = users_keys.map(function(key) {
			return users[key];
		});
		var index_user =  user_values.indexOf(socket.id);
		var doctor_name = users_keys[index_user];
		
		
		
		var newPatientToDoctor = userModel.findOneAndUpdate(
			{name: doctor_name},
			{$push: {patients: data.patient_name}},
			{
				new:true,
				upsert:true
			},
			function(err, patient) {
					if(err) return err;
			}

		);
		
		
	});
	
	
	socket.on('load patients server', function(data){	
		console.log("Load patients server");	
		var users_keys = Object.keys(users);
		var user_values = users_keys.map(function(key) {
			return users[key];
		});
		var index_user =  user_values.indexOf(socket.id);
		var doctor_name = users_keys[index_user];
		
		
		
		userModel.find(
			{name: doctor_name},
			function(err, userFound){
				if(err) return handleError(err);
				var patientsFound = userFound[0]["patients"];
				if(patientsFound === null) {
					patientsFound = "El doctor " + doctor_name + " no tiene pacientes";
					console.log("El doctor " + doctor_name + " no tiene pacientes");	
				} 
				
				console.log("patientsFound " + patientsFound);	

				socket.emit("load patients client", {listPatient: patientsFound});
			}
		);
		
		
	});
	
	
	socket.on('load recordings server', function(data){	
		console.log("Load recordings server. Patient: ", data.patient_name);	

		
		recordingModel.find(
			{patient_name: data.patient_name},
			function(err, recordingsFound){
				if(err){
					console.log("El paciente no tiene grabaciones");	
					return (err);
				}
				if(recordingsFound === null) {
					console.log("El paciente no tiene grabaciones");	
				} 
				
				console.log("recordingsFound " + recordingsFound);	

				socket.emit("load recordings client", {listRecordings: recordingsFound});
			}
		);
		
		
	});
	
		
	socket.on('load recording data server', function(data){	
		console.log("Load recording data server. Path: ", data.recording_path);
		var bytes_size = getFilesizeInBytes(data.recording_path);
		var data_buffer = []; // List of Buffer objects

		var bufArr = new ArrayBuffer(1);
		var bufView = new Uint8Array(bufArr);
		
		var i = 0;
		/*
		fs.open(data.recording_path, 'r', function(status, fd) {
			if (status) {
				console.log(status.message);
				return;
			}
			var buffer = new Buffer(100);
			fs.read(fd, buffer, 0, 100, 0, function(err, num) {
				console.log(buffer.toString('utf8', 0, num));
			});
		});
		*/
		var readStream = fs.createReadStream(data.recording_path);
		readStream.on('data', function (chunk) {
		  console.log(chunk.length);
			data_buffer.push(chunk);
		}).on('end', function() {
			data_buffer = Buffer.concat(data_buffer); // Make one large Buffer of it

			socket.emit("load recording data client", {recording_data: data_buffer});
		});
		
		
	});
	

	socket.on('load incidences server', function(data){	
		console.log("Load incidences server. Patient: ", data.recording_id);	

		
		recordingModel.find(
			{_id: data.recording_id},
			function(err, recordingFound){
				var incidencesFound = recordingFound[0]["incidences"];
				if(err){
					console.log("El paciente no tiene incidences");	
					return (err);
				}
				if(incidencesFound === null) {
					console.log("El paciente no tiene incidences");	
				} 
				
				console.log("incidencesFound " + incidencesFound);	

				socket.emit("load incidences client", {listIncidences: incidencesFound});
			}
		);
		
		
	});
	
	
	/*
	The easiest way rather than sending directly to the socket, would be creating a room for the 2 users to use and just send messages freely in there.
	socket.join('some-unique-room-name'); // Do this for both users you want to chat with each other
	socket.broadcast.to('the-unique-room-name').emit('message', 'blah'); // Send a message to the chat room
	*/
	socket.on('request transmission server', function(data){
		var patient_socket_id = users[data.patient_name];
		console.log('request transmission server');
		console.log("Doctor: ", socket.id);
		console.log("Patient: ", data.patient_name, " with id ", patient_socket_id);
		var data_to_client = {};
		data_to_client["doctor_socket_id"] = socket.id;
		data_to_client["patient_socket_id"] = patient_socket_id;
		
		socket.broadcast.to(patient_socket_id).emit('request transmission client', {doctor_socket_id: socket.id});
	});

	
	socket.on('upload transmission server', function(data){		
		console.log('upload transmission server');
		console.log("Patient: ", socket.id);
		console.log("transmission: ", data.transmission[0]);

		var users_keys = Object.keys(users);
		var user_values = users_keys.map(function(key) {
			return users[key];
		});
		var index_user =  user_values.indexOf(socket.id);
		var patient_name = users_keys[index_user];
		
		
		var doc_found = false;
		recordingModel.find(
			{patient_id: socket.id},
			function(err, recordingFound){
				if(err || !recordingFound.length) {
					console.log("DOC creating......");

					doc_found = false;
					var newRecording = recordingModel.findOneAndUpdate(
					{
						patient_id: socket.id
					},
					{			
						patient_name: patient_name,
						date: new Date(),
						recording: socket.id + + "_" + patient_name + ".data"
					},
					{
						new:true,
						upsert:true
					},
						function(err, doc) {
								if(err) return handleError(err);
								
							var date_normal = new Date();
							var date_ms = date_normal.getTime();
							var updateRecording = recordingModel.findOneAndUpdate(
								{
									_id: doc._id
								},
								{			
									recording:  doc._id + ".data"
								},
								{
									new:false,
									upsert:false
								},
								function(err, doc) {
										console.log("recording path updated");
										if(err) return handleError(err);
										fs.appendFile(doc._id + ".data", data.transmission,  "binary",function(err) {
											if(err) {
												console.log(err);
											} else {
												console.log("The file was saved!");
											}
										});
										
								}

							);
							//===========================================
								
						}

					);	
				} else {
					console.log("DOC FOUND!!!! IS: ", doc_found);
					doc_found = true;
					console.log("recording path updated");
										if(err) return handleError(err);
										fs.appendFile(recordingFound[0].recording, data.transmission,  "binary",function(err) {
											if(err) {
												console.log(err);
											} else {
												console.log("The file was saved!");
											}
										});
				} 
			}
		);
		
	});	

	socket.on('emit transmission server', function(data){
		var doctor_socket_id = data.doctor_socket_id;
		
		console.log('emit transmission server');
		console.log("Patient: ", socket.id);
		console.log("Doctor: ", doctor_socket_id);
		console.log("transmission: ", data.transmission[0]);

		var users_keys = Object.keys(users);
		var user_values = users_keys.map(function(key) {
			return users[key];
		});
		var index_user =  user_values.indexOf(socket.id);
		var patient_name = users_keys[index_user];
		
		/*
		var bufArr = new ArrayBuffer(4);
            var bufView = new Uint8Array(bufArr);
            bufView[0]=6;
            bufView[1]=7;
            bufView[2]=8;
            bufView[3]=9;
		*/
		
		socket.broadcast.to(doctor_socket_id).emit('receive transmission client', {transmission: data.transmission});
	});	
	
	socket.on('emit ppm server', function(data){
		console.log("PPM: ", data.ppm);

		var doctor_socket_id = data.doctor_socket_id;
		var message_alert = "";
		var type_incidence = "";
		var send_alert = false;
		var users_keys = Object.keys(users);
		var user_values = users_keys.map(function(key) {
			return users[key];
		});
		console.log("users_keys: ", users_keys);
		var index_user =  user_values.indexOf(socket.id);
		console.log("index_user: ", index_user);
		var patient_name = users_keys[index_user];
		console.log("patient_name: ", patient_name);

		if (data.ppm >= 100) {
			message_alert = "Taquicardia del paciente: ";
			type_incidence = "Taquicardia";
			send_alert = true;
		}
		if (data.ppm <= 50) {
			message_alert = "Bradicardia del paciente: ";
			type_incidence = "Bradicardia";
			send_alert = true;
		}
		if (send_alert) {
			var pat_id = socket.id;
			var incidence = {};
			incidence["date_incidence"] = new Date();
			incidence["id_incidence"] = socket.id + " __ " + incidence["date_incidence"];
			incidence["value_incidence"] = data.ppm;
			incidence["type_incidence"] = type_incidence;
			
			var newIncidence = recordingModel.findOneAndUpdate(
				{patient_id: pat_id},
				{$push: {incidences: incidence}},
				{
					new:false,
					upsert:false
				},
				function(err, patient) {
						if(err) return err;
				}

			);
			
			
			
			message_alert += patient_name;
			console.log("PPM sent");
			socket.broadcast.to(doctor_socket_id).emit('receive ppm client', {ppm: data.ppm, patient_name:patient_name, message_alert: message_alert});
		}
	});	
	
});

function getFilesizeInBytes(filename) {
    const stats = fs.statSync(filename)
    const fileSizeInBytes = stats.size
    return fileSizeInBytes
}


